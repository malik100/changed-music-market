use master
;
go 



/*--------------------------------------------------------------*/
create database MusicDB2
on primary
(
-- 1) rows data logical file name
	name = 'MusicDB',
-- 2) rows data initial file size
	size = 12MB,
-- 3) rows data auto growth size
	filegrowth = 10MB,
-- 4) rows data maximum file size
	maxsize = 500MB, -- or unlimited
-- 5) rows data path and file name
	filename = 'local\MusicDB2.mdf'
)
log on
(
	-- 1) log logical file logical name
	name = 'MusicDB2',
	-- 2) log initial file size (1/4 pf data file size)
	size = 3MB,
	-- 3) log auto growth size
	filegrowth = 10%,
	-- 4) log maximum file size
	maxsize = 25MB,
	-- 5) log path file name
	filename = 'local\MusicDB2_log.ldf'
)
;
go
/*--------------------------------------------------------------*/

use MusicDB2;
go

create table Songs	--COMPLETE
(
	Id int identity not null primary key,
	SongPrice decimal(5,2) not null ,
	SongName nvarchar(60) not null,
	Genre nvarchar(40) not null,
	ArtistId int not null,
	AlbumId int null
)
;
go

create table Albums   --COMPLETE
(
	Id int identity not null primary key,
	AlbumPrice decimal(5,2) not null ,
	AlbumName nvarchar(60) not null,
	ReleaseDate DateTime not null,
	ArtistId int not null
)
;
go

create table Artists	--COMPLETE
(
	Id int identity not null primary key,
	ArtistName nvarchar(60) not null
)
;
go



create table PaymentInfos --COMPLETE
(
	Id int identity not null primary key,
	CardholderName nvarchar(60) not null,
	ZipCode nvarchar(7) not null,
	ExpirationDate DateTime not null,
	CardNumber int not null,
	CustomerId int not null
)
;
go
create table Transactions   --COMPLETE
(
	Id int identity not null primary key,
	TransactionDate DateTime not null,
	CustomerId int not null,
	PaymentInfoId int not null,
)
;
go
--TABLE TRANSACTIONS-ALBUMS
create table TransactionAlbums  --done?
(
	TransactionId int not null, --FK PK-composite--/////////////
	AlbumId int not null --FK PK-composite--////////////////////
	constraint pk_TransactionsAlbums primary key clustered (TransactionId asc, AlbumId asc)
)
;
go
--TABLE TRANSACTIONS SONGS
create table TransactionSongs  --done?
(
	TransactionId int not null, --FK PK-composite////
	SongId int not null --FK PK-composite////////////
	constraint pk_TransactionsSongs primary key clustered (TransactionId asc, SongId asc)
)
;
go
--////////////////////////////////////////////////
create table Customers
(
	Id int identity not null primary key,
	CustomerName nvarchar(60) not null,
)
;
go
--TABLE CUSTOMER-ALBUMS--------------------------------------
create table CustomerAlbums  --done?
(
	CustomerId int not null, --FK PK-composite/////////
	AlbumId int not null --FK PK-composite////////
	constraint pk_CustomerAlbums primary key clustered (CustomerId asc, AlbumId asc)
)
;
go
--TABLE CUSTOMER-SONGS---------------------------------------
create table CustomerSongs --done?
(
	CustomerId int not null, --FK PK-composite/////////
	SongId int not null --FK PK-composite
	constraint pk_CustomerSongs primary key clustered (CustomerId asc, SongId asc)
)
;
go
--TABLE LISTENINGRECORDS-------------------------------------
create table ListeningRecords
(
	Id int identity not null primary key,
	DateOf DateTime not null,
	CustomerId int not null,
	SongId int not null
)
;
go

/*
--TABLE CUSTOMER-LISTENINGRECORDS----------------------------- !DROPPED
create table CustomerListeningRecords	--DONE?
(
	CustomerId int not null, --FK & PK-composite
	ListeningRecordId int not null --FK PK-composite
	constraint pk_CustomerListeningRecords primary key clustered (CustomerId asc, ListeningRecordId asc)
)
;
go
*/


--TABLE USERS-------------------------------------
create table Users
(
	Id int identity not null primary key,
	UserName nvarchar(60) not null,
	UserPassword nvarchar(60) not null,
	CustomerId int not null
)
;
go
--TABLE SongData-------------------------------------
create table SongData
(
	Id int identity not null primary key,
	SongBytes varbinary(max) not null,
	SongId int not null
)
;
go

--TABLE AlbumCovers-------------------------------------
create table AlbumCovers
(
	Id int identity not null primary key,
	AlbumCover varbinary(max) not null,
	AlbumId int not null
)
;
go
--////////////////////////////////////////////////////////////



alter table Users
add constraint fk_Users_Customers foreign key (CustomerId)
references Customers (Id)
;
go

alter table Songs
add constraint fk_Songs_Artists foreign key (ArtistId)
references Artists (Id)
;
go

alter table Songs
add constraint fk_Songs_Albums foreign key (AlbumId)
references Albums (Id)
;
go

alter table Albums
add constraint fk_Albums_Artists foreign key (ArtistId)
references Artists (Id)
;
go

	
alter table ListeningRecords
add constraint fk_ListeningRecords_Customers foreign key (CustomerId)
references Customers (Id)
;
go

alter table ListeningRecords
add constraint fk_ListeningRecords_Songs foreign key (SongId)
references Songs (Id)
;
go
	
alter table Transactions
add constraint fk_Transactions_Customers foreign key (CustomerId)
references Customers (Id)
;
go	

alter table Transactions
add constraint fk_Transactions_PaymentInfos foreign key (PaymentInfoId)
references PaymentInfos (Id)
;
go	

alter table PaymentInfos
add constraint fk_PaymentInfos_Customers foreign key (CustomerId)
references Customers (Id)
;
go	
/*
alter table CustomerListeningRecords
add constraint fk_CustomerListeningRecords_Customers foreign key (CustomerId)
references Customers (Id)
;
go

alter table CustomerListeningRecords
add constraint fk_CustomerListeningRecords_ListeningRecords foreign key (ListeningrecordId)
references ListeningRecords (Id)
;
go
*/
alter table CustomerSongs
add constraint fk_CustomerSongs_Customers foreign key (CustomerId)
references Customers (Id)
;
go


alter table CustomerSongs
add constraint fk_CustomerSongs_Songs foreign key (SongId)
references Songs (Id)
;
go

alter table 
CustomerAlbums
add constraint fk_CustomerAlbums_Customers foreign key (CustomerId)
references Customers (Id)
;
go

alter table 
CustomerAlbums
add constraint fk_CustomerAlbums_Albums foreign key (AlbumId)
references Albums (Id)
;
go



alter table TransactionSongs
add constraint fk_TransactionSongs_Transactions foreign key (TransactionId)
references Transactions (Id)
;
go

alter table TransactionSongs
add constraint fk_TransactionSongs_Songs foreign key (SongId)
references Songs (Id)
;
go


alter table TransactionAlbums
add constraint fk_TransactionAlbums_Transactions foreign key (TransactionId)
references Transactions (Id)
;
go

alter table TransactionAlbums
add constraint fk_TransactionAlbums_Albums foreign key (AlbumId)
references Albums (Id)
;
go



