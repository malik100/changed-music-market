﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using System.Windows.Controls.Primitives;
using System.Data;
using System.IO;

namespace SoundPlayer
{
	/// <summary>
	/// Interaction logic for MainWindow.xaml
	/// </summary>
	public partial class MainWindow : Window
	{
		//init
		DatabaseFirstDemoEntities ctx;
		private MediaPlayer mediaPlayer = new MediaPlayer();
		private bool mediaPlayerIsPlaying = false;
		private bool userIsDraggingSlider = false;

		// main window
		public MainWindow()
		{
			try
			{
				InitializeComponent();
				ctx = new DatabaseFirstDemoEntities();
				lvSongList.ItemsSource = ctx.SongLists.ToList<SongList>();

				//picture backgroud choose
				ImageBrush myBrush = new ImageBrush();
				myBrush.ImageSource = new BitmapImage(new Uri(@"..\..\Images\sound-waves.jpg", UriKind.Relative));
				myBrush.TileMode = TileMode.Tile;
				myBrush.Stretch = Stretch.Fill;
				//lblStatus.Background = myBrush;

				//mp3 source choose
				OpenFileDialog openFileDialog = new OpenFileDialog();
				openFileDialog.Filter = "MP3 files (*.mp3)|*.mp3|All files (*.*)|*.*";
				if (openFileDialog.ShowDialog() == true)
				{
					//mediaPlayer.Open(new Uri(openFileDialog.FileName));
					//store the mp3 into db
					//MessageBox.Show(openFileDialog.FileName);
					databaseFilePut(openFileDialog.FileName);
				}

				//timer defination
				DispatcherTimer timer = new DispatcherTimer();
				timer.Interval = TimeSpan.FromSeconds(1);
				timer.Tick += timer_Tick;
				timer.Start();
			}
			catch (SystemException ex)
			{
				Console.WriteLine(ex.StackTrace);
				MessageBox.Show("Fatal error: Database connection failed:\n" + ex.Message);
				Environment.Exit(1); // fatal error
			}
		}

		//method 1: convert mp3 to Byte[] and save to db
		public static void databaseFilePut(string varFilePath)
		{
			byte[] file;
			using (var stream = new FileStream(varFilePath, FileMode.Open, FileAccess.Read))
			{
				using (var reader = new BinaryReader(stream))
				{
					file = reader.ReadBytes((int)stream.Length);
				}
			}
			// got the mp3 name
			string[] data = varFilePath.Split('\\');
			string mp3name = data[data.Length - 1];
			// add the data to DB
			using (var ctx = new DatabaseFirstDemoEntities())
			{
				SongList songList = new SongList
				{
					SongType = "Pop",
					SongData = file,
					SongName = mp3name
				};
				ctx.SongLists.Add(songList);
				ctx.SaveChanges();
			}
		}

		//method 2:find the data from db and convert Byte[] to mp3


		private void timer_Tick(object sender, EventArgs e)
		{
			//if (mediaPlayer.Source != null)
			//	lblStatus.Content = String.Format("{0} / {1}", mediaPlayer.Position.ToString(@"mm\:ss"), mediaPlayer.NaturalDuration.TimeSpan.ToString(@"mm\:ss"));
			//else
			//	lblStatus.Content = "No file selected...";
			if ((mediaPlayer.Source != null) && (mediaPlayer.NaturalDuration.HasTimeSpan) && (!userIsDraggingSlider))
			{
				sliProgress.Minimum = 0;
				sliProgress.Maximum = mediaPlayer.NaturalDuration.TimeSpan.TotalSeconds;
				sliProgress.Value = mediaPlayer.Position.TotalSeconds;
			}
		}

		private void btnPlay_Click(object sender, RoutedEventArgs e)
		{
			mediaPlayer.Play();
		}

		private void btnPause_Click(object sender, RoutedEventArgs e)
		{
			mediaPlayer.Pause();
		}

		private void btnStop_Click(object sender, RoutedEventArgs e)
		{
			mediaPlayer.Stop();
		}

		private void sliProgress_DragStarted(object sender, DragStartedEventArgs e)
		{
			userIsDraggingSlider = true;
		}

		private void sliProgress_DragCompleted(object sender, DragCompletedEventArgs e)
		{
			userIsDraggingSlider = false;
			mediaPlayer.Position = TimeSpan.FromSeconds(sliProgress.Value);
		}

		private void sliProgress_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
		{
			lblProgressStatus.Text = TimeSpan.FromSeconds(sliProgress.Value).ToString(@"hh\:mm\:ss");
		}

		private void SongStartClick(object sender, MouseButtonEventArgs e)
		{
			SongList selectItem = lvSongList.SelectedItem as SongList;
			//MessageBox.Show(selectItem.ToString())
			//File.WriteAllBytes(@"../../audio/mp3File.mp3", selectItem.SongData);
			mediaPlayer.Open(new Uri(@"../../audio/" + selectItem.SongName, UriKind.Relative));
			mediaPlayer.Play();
		}

        private void AddSongClick(object sender, RoutedEventArgs e)
        {
			addSongForAdminDlg dlg = new addSongForAdminDlg();
			dlg.Owner = this;
			if (dlg.ShowDialog() == true)
			{
				MessageBox.Show("Its done!");
			}
		}
    }
}